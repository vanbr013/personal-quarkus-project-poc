1º Rename source project name to "ifood";
2º Create a postgresql 11+ connection "localhost:5432/cadastro-db";
3º Alter in application.properties, the config "quarkus.hibernate-orm.database.generation=none" to "=drop-and-create" for the first time running the app.;
4º Go to "http://localhost:8080/q/swagger-ui/" in your browser.
