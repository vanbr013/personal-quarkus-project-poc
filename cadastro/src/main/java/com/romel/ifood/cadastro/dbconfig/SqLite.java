//package com.romel.ifood.cadastro.dbconfig;
//
//import io.agroal.api.AgroalDataSource;
//import io.quarkus.agroal.DataSource;
//
//import javax.enterprise.context.ApplicationScoped;
//import javax.inject.Inject;
//import java.sql.Connection;
//import java.sql.SQLException;
//
//@ApplicationScoped
//public class SqLite {
//
//    @Inject
//    @DataSource("cadastro")
//    AgroalDataSource sqliteDbJasmarty;
//
//    private static Connection cjasmarty = null;
//
//    /**
//     * @return
//     * @throws SQLException
//     */
//    public Connection getDbJasmarty() throws SQLException {
//        if (cjasmarty == null) {
//            cjasmarty = sqliteDbJasmarty.getConnection();
//        }
//        return cjasmarty;
//    }
//}
