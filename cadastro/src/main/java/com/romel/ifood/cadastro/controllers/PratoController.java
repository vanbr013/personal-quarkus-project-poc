package com.romel.ifood.cadastro.controllers;

import com.romel.ifood.cadastro.models.dtos.prato.AdicionarPratoDTO;
import com.romel.ifood.cadastro.models.dtos.prato.AtualizarPratoDTO;
import com.romel.ifood.cadastro.models.dtos.prato.PratoDTO;
import com.romel.ifood.cadastro.models.dtos.prato.PratoMapper;
import com.romel.ifood.cadastro.models.entities.PratoEntity;
import com.romel.ifood.cadastro.models.entities.RestauranteEntity;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/pratos")
@Tag(name = "Prato")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PratoController {

    @Inject
    PratoMapper pratoMapper;

    @GET
    @Path("{idRestaurante}")
    public List<PratoDTO> buscar(@PathParam("idRestaurante") Long idRestaurante) {
        Optional<RestauranteEntity> restauranteEntityOptional =
                RestauranteEntity.findByIdOptional(idRestaurante);
        if (restauranteEntityOptional.isEmpty())
            throw new NotFoundException("Restaurante não existe");

        Stream<PratoEntity> pratoEntityStream =
                PratoEntity.find("restaurante_id",idRestaurante).stream();
        return pratoEntityStream.map(p -> pratoMapper.pratoEntitytoPratoDTO(p)).collect(Collectors.toList());
    }

    @POST
    @Path("{idRestaurante}")
    @Transactional
    public Response adicionar(AdicionarPratoDTO adicionarPratoDTO, @PathParam("idRestaurante") Long idRestaurante) {
        Optional<RestauranteEntity> restauranteEntityOptional =
                RestauranteEntity.findByIdOptional(idRestaurante);
        if (restauranteEntityOptional.isEmpty())
            throw new NotFoundException("Restaurante não existe");

        PratoEntity pratoEntity = pratoMapper.adicionarPratoDTOToPratoEntity(adicionarPratoDTO);
        pratoEntity.setRestauranteEntity(restauranteEntityOptional.get());
        PratoEntity.persist(pratoEntity);
        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("{idRestaurante}/{id}")
    @Transactional
    public void atualizar(AtualizarPratoDTO atualizarPratoDTO, @PathParam("id") Long id,
                          @PathParam("idRestaurante") Long idRestaurante) {
        Optional<RestauranteEntity> restauranteEntityOptional =
                RestauranteEntity.findByIdOptional(idRestaurante);
        if (restauranteEntityOptional.isEmpty())
            throw new NotFoundException("Restaurante não existe");

        Optional<PratoEntity> pratoEntityOptional = PratoEntity.findByIdOptional(id);

        if (pratoEntityOptional.isEmpty())
            throw new NotFoundException("Prato não existe");

        PratoEntity pratoEntityDB = pratoEntityOptional.get();
        pratoMapper.atualizarPratoDTOToPratoEntity(atualizarPratoDTO, pratoEntityDB);
        PratoEntity.persist(pratoEntityDB);
    }

    @DELETE
    @Path("{idRestaurante}/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id, @PathParam("idRestaurante") Long idRestaurante) {
        Optional<RestauranteEntity> restauranteEntityOptional =
                RestauranteEntity.findByIdOptional(idRestaurante);
        if (restauranteEntityOptional.isEmpty())
            throw new NotFoundException("Restaurante não existe");

        Optional<PratoEntity> optionalPratoEntity =
                PratoEntity.findByIdOptional(id);

        optionalPratoEntity.ifPresentOrElse(PratoEntity::delete, () -> {
            throw new NotFoundException("Prato inexistente");
        });
    }
}
