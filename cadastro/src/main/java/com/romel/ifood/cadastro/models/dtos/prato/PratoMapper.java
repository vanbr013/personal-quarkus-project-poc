package com.romel.ifood.cadastro.models.dtos.prato;

import com.romel.ifood.cadastro.models.dtos.restaurante.RestauranteDTO;
import com.romel.ifood.cadastro.models.entities.PratoEntity;
import com.romel.ifood.cadastro.models.entities.RestauranteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "cdi")
public interface PratoMapper {

//    @Mapping(target = "nome", source = "nomeFantasia")
//    @Mapping(target = "localizacaoEntity", source = "localizacaoDTO")
    PratoEntity adicionarPratoDTOToPratoEntity(AdicionarPratoDTO dto);

//    @Mapping(target = "nome", source = "nomeFantasia")
    PratoEntity atualizarPratoDTOToPratoEntity
    (AtualizarPratoDTO dto, @MappingTarget PratoEntity pratoEntity);

    @Mapping(target = "nomeDoPrato", source = "nome")
    PratoDTO pratoEntitytoPratoDTO(PratoEntity pratoEntity);



}
