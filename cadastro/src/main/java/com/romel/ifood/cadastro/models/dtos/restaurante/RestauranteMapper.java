package com.romel.ifood.cadastro.models.dtos.restaurante;

import com.romel.ifood.cadastro.models.entities.RestauranteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "cdi")
public interface RestauranteMapper {

//    @Mapping(target = "nome", source = "nomeFantasia")
//    @Mapping(target = "localizacaoEntity", source = "localizacaoDTO")
    RestauranteEntity adicionarRestauranteDTOToRestauranteEntity(AdicionarRestauranteDTO dto);

//    @Mapping(target = "nome", source = "nomeFantasia")
    RestauranteEntity atualizarRestauranteDTOToRestauranteEntity
    (AtualizarRestauranteDTO dto, @MappingTarget RestauranteEntity restauranteEntity);

    RestauranteEntity adicionarRestauranteDTODTOToRestauranteEntity(AdicionarRestauranteDTO dto);

    //    @Mapping(target = "dataCriacao", dateFormat = "dd/MM/yyyy HH:mm:ss")
    RestauranteDTO restauranteEntitytoRestauranteDTO(RestauranteEntity restauranteEntity);



}
