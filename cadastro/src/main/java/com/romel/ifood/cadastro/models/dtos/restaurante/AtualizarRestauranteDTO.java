package com.romel.ifood.cadastro.models.dtos.restaurante;

public class AtualizarRestauranteDTO {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
