package com.romel.ifood.cadastro.models.dtos.restaurante;


import com.romel.ifood.cadastro.models.dtos.LocalizacaoDTO;

public class RestauranteDTO {

    private String proprietario;

    private String cnpj;

    private String nome;

    private LocalizacaoDTO localizacao;

    private String dataCriacao;

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalizacaoDTO getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(LocalizacaoDTO localizacaoDTO) {
        this.localizacao = localizacaoDTO;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

}
