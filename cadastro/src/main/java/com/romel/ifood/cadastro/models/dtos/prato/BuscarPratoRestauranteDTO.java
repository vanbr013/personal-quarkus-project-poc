package com.romel.ifood.cadastro.models.dtos.prato;

import com.romel.ifood.cadastro.models.dtos.LocalizacaoDTO;

public class BuscarPratoRestauranteDTO {

    private String cnpj;

    private String nome;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
