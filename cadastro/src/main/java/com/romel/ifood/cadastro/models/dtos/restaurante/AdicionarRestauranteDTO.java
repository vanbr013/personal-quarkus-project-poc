package com.romel.ifood.cadastro.models.dtos.restaurante;

import com.romel.ifood.cadastro.models.dtos.LocalizacaoDTO;
import com.romel.ifood.cadastro.models.entities.LocalizacaoEntity;

public class AdicionarRestauranteDTO {

    private String proprietario;

    private String cnpj;

    private String nome;

    private LocalizacaoDTO localizacao;

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalizacaoDTO getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(LocalizacaoDTO localizacaoDTO) {
        this.localizacao = localizacaoDTO;
    }

}
