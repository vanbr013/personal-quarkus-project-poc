package com.romel.ifood.cadastro.models.dtos.prato;

import java.math.BigDecimal;

public class PratoDTO {

    private String nomeDoPrato;

    private String descricao;

    private BuscarPratoRestauranteDTO restaurante;

    private BigDecimal preco;

    public String getNomeDoPrato() {
        return nomeDoPrato;
    }

    public void setNomeDoPrato(String nomeDoPrato) {
        this.nomeDoPrato = nomeDoPrato;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BuscarPratoRestauranteDTO getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(BuscarPratoRestauranteDTO restaurante) {
        this.restaurante = restaurante;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }
}
