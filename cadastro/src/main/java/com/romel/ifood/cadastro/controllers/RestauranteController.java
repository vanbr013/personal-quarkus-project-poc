package com.romel.ifood.cadastro.controllers;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.romel.ifood.cadastro.models.dtos.restaurante.AdicionarRestauranteDTO;
import com.romel.ifood.cadastro.models.dtos.restaurante.AtualizarRestauranteDTO;
import com.romel.ifood.cadastro.models.dtos.restaurante.RestauranteDTO;
import com.romel.ifood.cadastro.models.dtos.restaurante.RestauranteMapper;
import com.romel.ifood.cadastro.models.entities.LocalizacaoEntity;
import com.romel.ifood.cadastro.models.entities.RestauranteEntity;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/restaurantes")
@Tag(name = "Restaurante")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestauranteController {


    @Inject
    RestauranteMapper restauranteMapper;

    @GET
    public List<RestauranteDTO> buscar() throws SQLException {
        Stream<RestauranteEntity> restauranteEntityStream = RestauranteEntity.findAll().stream();
        return restauranteEntityStream.
                map(r -> restauranteMapper.restauranteEntitytoRestauranteDTO(r)).collect(Collectors.toList());
    }

    @POST
    @Transactional
    public Response adicionar(AdicionarRestauranteDTO adicionarRestauranteDTO) {
        RestauranteEntity restaurante = restauranteMapper.
                adicionarRestauranteDTOToRestauranteEntity(adicionarRestauranteDTO);

        LocalizacaoEntity localizacaoEntity = new LocalizacaoEntity();
        localizacaoEntity.setLatitude(adicionarRestauranteDTO.getLocalizacao().getLatitude());
        localizacaoEntity.setLongitude(adicionarRestauranteDTO.getLocalizacao().getLongitude());
        restaurante.setLocalizacao(localizacaoEntity);

        LocalizacaoEntity.persist(localizacaoEntity);
        RestauranteEntity.persist(restaurante);

        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public void atualizar(AtualizarRestauranteDTO restauranteDTO, @PathParam("id") Long id) {
        Optional<RestauranteEntity> restauranteEntityOptional =
                RestauranteEntity.findByIdOptional(id);
        if (restauranteEntityOptional.isEmpty())
            throw new NotFoundException();

        RestauranteEntity restauranteEntityDB = restauranteEntityOptional.get();
        restauranteMapper.atualizarRestauranteDTOToRestauranteEntity(restauranteDTO, restauranteEntityDB);

        RestauranteEntity.persist(restauranteEntityDB);
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void deletar(@PathParam("id") Long id) {
        Optional<RestauranteEntity> restauranteEntityOptional =
                RestauranteEntity.findByIdOptional(id);

        restauranteEntityOptional.ifPresentOrElse(RestauranteEntity::delete,
                () -> {
                    throw new NotFoundException();
                });

    }
}
