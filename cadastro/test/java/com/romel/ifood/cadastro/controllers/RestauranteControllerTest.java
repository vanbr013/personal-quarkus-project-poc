package com.romel.ifood.cadastro.controllers;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;
import com.github.database.rider.core.api.dataset.DataSet;
import com.romel.ifood.cadastro.CadastroTestLifecycleManager;
import com.romel.ifood.cadastro.models.entities.RestauranteEntity;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.approvaltests.Approvals;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import java.util.Optional;


@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(CadastroTestLifecycleManager.class)
public class RestauranteControllerTest {

    @Test
    @DataSet("restaurantes-cenario-1.yml")
    public void testBuscarRestaurantes() {
        String resultado = given()
                .when().get("/restaurantes")
                .then()
                .statusCode(200)
                .extract().asString();
        Approvals.verifyJson(resultado);
    }

    private RequestSpecification given() {
        return RestAssured.given().contentType(ContentType.JSON);
    }

    @Test
    @DataSet("restaurantes-cenario-1.yml")
    public void testAlterarNomeRestaurante() {
        RestauranteEntity restauranteEntity = new RestauranteEntity();
        restauranteEntity.setNome("novoNome");
        Long parameterValue = 123L;
        given()
                .with().pathParam("id", parameterValue)
                .body(restauranteEntity)
                .when().put("/restaurantes/{id}")
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode())
                .extract().asString();

        Optional<RestauranteEntity> byId = RestauranteEntity.findByIdOptional(parameterValue);

        Assertions.assertEquals(restauranteEntity.getNome(), byId.get().getNome());
    }
}